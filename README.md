# Generic subcvmfs-builder-pipeline


## Purpose

`subcvmfs-builder-pipeline` aims to build a subset a CVMFS containing the minimal requirements to execute applications of interests and deploy it on a remote server.
`subcvmfs-builder-pipeline` is a simple example that can be used as a template to create a solution tailored for your needs.
The tool relies on `subcvmfs-builder` and is mainly used to supply supercomputers having no outbound connectivity with workload dependencies.

## Structure

![schema](SubCVMFS.png)

- `main.sh` is the main file:
  - It installs the requirements (`cvmfs`, `cvmfs-shrinkwrap`, `subcvmfs-builder`).
  - It executes sequentially the steps of `subcvmfs-builder`: `Trace`, `Build`, `Test`, `Deploy`.

- `configuration.json` provides inputs to `main.sh` (and `subcvmfs-builder`). It must contain the CVMFS respositories and may comprise a container name and other options.

- `.gitlab-ci.yml` is used to execute `main.sh` in a specific environment.

Further details about the context can be found in: https://arxiv.org/abs/2303.16675