# v2.0.1

- CHANGE: use mambaforge instead of miniconda

# v2.0.0

- CHANGE: use subcvmfs-builder package

# v1.1.0

- ADD: documentation to setup a runner
- ADD: a json configuration, make the project more generic

# v1.0.2

- ADD: an inputs directory containing gauss configuration files

# v1.0.1

- CHANGE: refactor the steps

# v1.0.0

- ADD: steps to configure, build, test, deploy
