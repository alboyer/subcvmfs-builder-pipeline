###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from ProdConf import ProdConf

# NOfEvents=42,

ProdConf(
  Application='Gauss',
  AppVersion='v49r21',
  OutputFilePrefix='00146702_00006948_1',
  OutputFileTypes=['sim'],
  XMLSummaryFile='summaryGauss_00146702_00006948_1.xml',
  XMLFileCatalog='pool_xml_catalog.xml',
  DDDBTag='dddb-20170721-3',
  CondDBTag='sim-20170721-2-vc-mu100',
  NOfEvents=1,
  RunNumber=14677148,
  FirstEventNumber=3035840,
)
