###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
source /cvmfs/lhcb.cern.ch/lib/LbEnv.sh
lb-run --unset LD_LIBRARY_PATH --unset PYTHONPATH --unset XrdSecPROTOCOL --siteroot=/cvmfs/lhcb.cern.ch/lib/ --allow-containers -c x86_64-slc6-gcc48-opt --use="AppConfig v3r395" --use="Gen/DecFiles v30r62" --use="ProdConf" Gauss/v49r21 gaudirun.py -T '$APPCONFIGOPTS/Gauss/Beam6500GeV-mu100-2016-nu1.6.py' '$APPCONFIGOPTS/Gauss/EnableSpillover-25ns.py' '$APPCONFIGOPTS/Gauss/DataType-2016.py' '$APPCONFIGOPTS/Gauss/RICHRandomHits.py' '$DECFILESROOT/options/13104013.py' '$LBPYTHIA8ROOT/options/Pythia8.py' '$APPCONFIGOPTS/Gauss/G4PL_FTFP_BERT_EmNoCuts.py' '$APPCONFIGOPTS/Persistency/Compression-ZLIB-1.py' prodConf_Gauss_00146702_00006948_1.py
