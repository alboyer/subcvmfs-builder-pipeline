# Configure a runner

Because subsets of CVMFS can be heavy and the pipeline might take hours to run from end to end,
it is highly recommended to setup and use your own gitlab runner.

## Docker

- Install docker

```bash
sudo yum remove docker \
                docker-client \
                docker-client-latest \
                docker-common \
                docker-latest \
                docker-latest-logrotate \
                docker-logrotate \
                docker-engine
                
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install docker-ce docker-ce-cli containerd.io
sudo systemctl start docker
sudo docker run hello-world
```

- Make it runnable for non-root users

```bash
sudo groupadd docker
sudo usermod -aG docker $USER
```

## Gitlab Runner

- Install Gitlab Runner

```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
sudo yum install gitlab-runner
```

- Register the runner

```bash
sudo gitlab-runner register \
  --non-interactive \ 
  --executor "docker" \
  --docker-image alpine:latest \
  --url "https://gitlab.cern.ch/" \
  --registration-token <token> \
  --description "docker-runner" \
  --tag-list "docker-privileged" \
  --run-untagged="true" \
  --locked="true"
```

- Create a directory for CVMFS (You might need to mount a volume and make it shareable with `mount --make-shared /shared-mounts`):

```bash
mkdir /shared-mounts
```

- Modify `/etc/gitlab-runner/config.toml` (the file is hidden)

```bash
sudo vi /etc/gitlab-runner/config.toml
```

- Add the following sections

```yaml
[runners.docker]
  image = "docker:19.03.12"
  privileged = true
  volumes = ["/shared-mounts:/shared-mounts:rshared", "/certs/client", "/cache"]
```

## Utils

### Get more space to deal with large subset of CVMFS

- Get additional space for docker:

```bash
sudo systemctl stop docker service
sudo vi /etc/docker/daemon.json
sudo systemctl stop docker service
```

- Add the following section to `/etc/docker/daemon.json`:

```json
{
"data-root": "/new/data/root/path"
}
```

### Delete an old runner

- Delete a runner:

```bash
sudo gitlab-runner list
sudo gitlab-runner verify --delete -t <token> -u http://git.xxxx.com/
```