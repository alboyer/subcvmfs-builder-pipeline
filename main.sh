#!/bin/bash
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
set -x
set -e

###############################################################################
# INSTALLATION STEPS
###############################################################################


mkdir -p mambaforge
curl -L -O "https://github.com/conda-forge/miniforge/releases/latest/download/Mambaforge-$(uname)-$(uname -m).sh"
bash Mambaforge-$(uname)-$(uname -m).sh -b -u -p mambaforge
rm -rf Mambaforge-$(uname)-$(uname -m).sh
export PATH="$PWD/mambaforge/bin:$PATH"

# install subcvmfs-builder
git clone https://gitlab.cern.ch/alboyer/subcvmfs-builder.git
cd subcvmfs-builder

eval "$(conda shell.bash hook)"
mamba env create --quiet --file environment.yml
conda activate subcvmfs-env
python3 -m pip install .
cd ..

# install dependencies
yum install -y tree
yum install -y https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest.noarch.rpm
yum install -y cvmfs-shrinkwrap

curl -O http://ccl.cse.nd.edu/software/files/cctools-7.5.4-x86_64-centos7.tar.gz
tar -zxf cctools-7.5.4-x86_64-centos7.tar.gz
mv cctools-7.5.4-x86_64-centos7.tar.gz-dir cctools

export PATH=$PWD/cctools/bin:$PATH
export PYTHONPATH=$PWD/cctools/lib/python3.7/site-packages:${PYTHONPATH}
export PERL5LIB=$PWD/cctools/lib/perl5/site_perl/5.16.3:${PERL5LIB}

# create a directory that will contain artifacts to upload
mkdir -p $PWD/artifacts

###############################################################################
# SUBCVMFS STEPS
###############################################################################

# Common variables
SUBCVMFS_PATH="subcvmfs"
APPS_DIR="inputs"

# Trace
subcvmfs trace $APPS_DIR \
  $PWD/cvmfs_configuration.json \
  "http://ca-proxy-lhcb.cern.ch:3128" "http://ca-proxy.cern.ch:3128" "DIRECT" \
  $PWD/artifacts/namelist.txt \
  --stdout $PWD/artifacts/trace.out \
  --stderr $PWD/artifacts/trace.err \
  --debug

# Build
subcvmfs build $PWD/cvmfs_configuration.json \
  $PWD/artifacts/namelist.txt $APPS_DIR/namelist.txt \
  $SUBCVMFS_PATH \
  --stdout $PWD/artifacts/build.out \
  --stderr $PWD/artifacts/build.err \
  --debug

tree $PWD/$SUBCVMFS_PATH > $PWD/artifacts/structure.txt

# Test
subcvmfs test $APPS_DIR $SUBCVMFS_PATH \
  --stdout $PWD/artifacts/test.out \
  --stderr $PWD/artifacts/test.err \
  --debug
